package homework8

val userCart = mutableMapOf<String, Int>("potato" to 2, "cereal" to 2, "milk" to 1, "sugar" to 3, "onion" to 1, "tomato" to 2, "cucumber" to 2, "bread" to 3)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    println( findOutNumberVegetablesInBasket(userCart, vegetableSet))
    println( calculateCostProducts(discountValue, discountSet, prices, userCart))

}

fun findOutNumberVegetablesInBasket (userCart: MutableMap<String, Int>, vegetableSet: Set<String>) : Int {
    var count = 0
    for (i in userCart) {
        if (vegetableSet.contains(i.key))
            count += i.value
    }
    return count
}

fun calculateCostProducts(discountValue: Double, discountSet: Set<String>, prices: MutableMap<String, Double>,
                          userCart: MutableMap<String, Int>) : Double {
    var cost : Double = 0.00
    for (i in userCart) {
        if (discountSet.contains(i.key))
            cost+= (1 - discountValue) * i.value * prices[i.key]!!
        else
            cost+= i.value * prices[i.key]!!
    }
    return cost
}

