package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)

    var joeCoins = 0
    var teamCoins = 0
    for (i in myArray) {
        if (i % 2 == 0)
            joeCoins++
        else
            teamCoins++
    }
//    2-ой вариант решения мне нравится больше, но не подходит под условия задачи
//    val joeCoins = myArray.count { it  % 2 == 0}
//    val teamCoins = myArray.size - joeCoins
    println("Монет у капитана Джо (четные) - $joeCoins, у команды (нечетные) - $teamCoins")
}
