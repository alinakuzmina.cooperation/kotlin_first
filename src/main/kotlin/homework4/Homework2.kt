package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)

    var excellentCount = 0
    var goodCount = 0
    var satisfactoryCount = 0
    var unsatisfactoryCount = 0
    val numberStudents = marks.size.toDouble()
    for (grade in marks) {
        when (grade) {
            5 -> excellentCount++
            4 -> goodCount++
            3 -> satisfactoryCount++
            2 -> unsatisfactoryCount++
        }
    }
    println("Отличников - ${excellentCount / numberStudents * 100}%")
    println("Хорошистов - ${goodCount / numberStudents * 100}%")
    println("Троечников - ${satisfactoryCount / numberStudents * 100}%")
    println("Двоечников  - ${unsatisfactoryCount / numberStudents * 100}%")
// 2-ой вариант решения
//    val numberStudents = marks.size.toDouble()
//    val percentageStudentsWith5 = marks.count{it == 5} / numberStudents * 100
//    val percentageStudentsWith4 = marks.count{it == 4} / numberStudents * 100
//    val percentageStudentsWith3 = marks.count{it == 3} / numberStudents * 100
//    val percentageStudentsWith2 = marks.count{it == 2} / numberStudents * 100
//    println("Отличников - $percentageStudentsWith5%")
//    println("Хорошистов - $percentageStudentsWith4%")
//    println("Троечников - $percentageStudentsWith3%")
//    println("Двоечников  - $percentageStudentsWith2%")
}